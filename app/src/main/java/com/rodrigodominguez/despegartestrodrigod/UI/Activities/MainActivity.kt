package com.rodrigodominguez.despegartestrodrigod.UI.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar

import com.rodrigodominguez.despegartestrodrigod.Constants
import com.rodrigodominguez.despegartestrodrigod.MVP.Contract.HotelsContract
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.ItemsItem
import com.rodrigodominguez.despegartestrodrigod.MVP.Presenters.HotelsPresenter
import com.rodrigodominguez.despegartestrodrigod.R
import com.rodrigodominguez.despegartestrodrigod.UI.Adapters.HotelListAdapter

class MainActivity : AppCompatActivity(), HotelsContract.MainView<List<ItemsItem>>, HotelListAdapter.OnClickHotelListener {

    private lateinit var presenter: HotelsContract.presenter
    private lateinit var recycler_hotels: RecyclerView
    private lateinit var progressBar: ProgressBar

    private var adapter: HotelListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_hotels = findViewById(R.id.recycler_hotels)
        progressBar = findViewById(R.id.progress_main)

        presenter = HotelsPresenter(this, this)
        presenter.getListHotels()

    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun finishService() {

    }

    override fun onSuccesResponse(itemsItems: List<ItemsItem>) {

        adapter = HotelListAdapter(this, itemsItems)

        adapter!!.setOnClickHotelListener(this)

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        recycler_hotels.layoutManager = layoutManager

        recycler_hotels.adapter = adapter
    }

    override fun onFailureResponse(error: String) {

    }

    override fun onClickHotel(hotel: ItemsItem) {
        startActivity(Intent(this, DetailHotelActivity::class.java).putExtra(Constants.BUNDLE_ID_HOTEL, hotel.id))
    }
}
