package com.rodrigodominguez.despegartestrodrigod.UI.Activities

import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView

import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import com.rodrigodominguez.despegartestrodrigod.Constants
import com.rodrigodominguez.despegartestrodrigod.MVP.Contract.HotelDetailContract
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.Comments
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.Hotel
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.HotelDetailParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.ReviewsItem
import com.rodrigodominguez.despegartestrodrigod.MVP.Presenters.HotelDetailPresenter
import com.rodrigodominguez.despegartestrodrigod.R
import com.rodrigodominguez.despegartestrodrigod.UI.Activities.CommentsActivity
import com.rodrigodominguez.despegartestrodrigod.UI.Adapters.AmenitiesAdapter
import com.rodrigodominguez.despegartestrodrigod.UI.Adapters.RatingStartAdapter

import java.util.ArrayList

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.rodrigodominguez.despegartestrodrigod.Constants.AMENITIES_EXTRA_KEY
import com.rodrigodominguez.despegartestrodrigod.Constants.IMAGE_COMMENTS_EXTRA_KEY
import com.rodrigodominguez.despegartestrodrigod.Constants.TYPE_ACTIVITY_DETAIL_AMENITIES
import com.rodrigodominguez.despegartestrodrigod.UI.Activities.ImageDetailActivity.IMAGE_URL_EXTRA_KEY
import kotlinx.android.synthetic.main.activity_detail_hotel.*

class DetailHotelActivity : AppCompatActivity(), HotelDetailContract.MainView<HotelDetailParentResponse>, View.OnClickListener {

    private lateinit  var presenter: HotelDetailContract.presenter

    private lateinit var hotel: Hotel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_hotel)

        container_header!!.setOnClickListener(this)
        comments!!.setOnClickListener(this)
        more_amenities!!.setOnClickListener(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        presenter = HotelDetailPresenter(this, this)
        presenter.getDetailHotel(intent.getStringExtra(Constants.BUNDLE_ID_HOTEL))
    }

    override fun showProgress() {
        progress_detail!!.visibility = View.VISIBLE
        container_info_parent!!.visibility = View.GONE
        container_price!!.visibility = View.GONE
    }

    override fun hideProgress() {
        progress_detail!!.visibility = View.GONE
        container_info_parent!!.visibility = View.VISIBLE
        container_price!!.visibility = View.VISIBLE
    }

    override fun finishService() {

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            presenter.onDestroy()
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSuccesResponse(hotelDetailParentResponse: HotelDetailParentResponse) {
        bindView(hotelDetailParentResponse.hotel)

        val priceText = (hotelDetailParentResponse.price.currency.mask + " "
                + hotelDetailParentResponse.price.base.toString())
        price_detail_activity!!.text = priceText

    }



    private fun bindView(hotel: Hotel) {
        this. hotel = hotel

        rating!!.text = hotel.rating.toString()
        description!!.text = hotel.description
        title_hotel!!.text = hotel.name

        val adapterStart = RatingStartAdapter(hotel.stars)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recycler_starts!!.layoutManager = layoutManager
        recycler_starts!!.adapter = adapterStart

        val layoutManageramenities = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        val adapteramenities = AmenitiesAdapter(hotel.amenities)
        adapteramenities.setTypeRecycler(Constants.TYPE_ACTIVITY_DETAIL_AMENITIES)
        recycler_amenities!!.layoutManager = layoutManageramenities
        recycler_amenities!!.adapter = adapteramenities

        val options = RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
        if (!this.isFinishing) {
            Glide.with(this)
                    .load(hotel.mainPicture)
                    .apply(options)
                    .transition(withCrossFade())
                    .into(main_image!!)
        }
    }

    override fun onFailureResponse(error: String) {

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.container_header -> {
                startActivity(Intent(this, ImageDetailActivity::class.java).putExtra(IMAGE_URL_EXTRA_KEY, hotel!!.mainPicture))
            }
            R.id.comments -> {
                startActivity(Intent(this, CommentsActivity::class.java).putExtra(Constants.IMAGE_COMMENTS_EXTRA_KEY, hotel!!.reviews))

            }
            R.id.more_amenities -> {
                startActivity(Intent(this, AmenitiesActivity::class.java).putExtra(Constants.AMENITIES_EXTRA_KEY, hotel!!.amenities))

            }
        }
    }

}
