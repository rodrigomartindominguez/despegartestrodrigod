package com.rodrigodominguez.despegartestrodrigod.MVP.DAOs

import android.content.Context

import com.rodrigodominguez.despegarcomtest.MVP.DAOs.BaseDAO
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.HotelDetailParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.HotelsListParentResponse

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import retrofit2.http.Path

class HotelsDAO(context: Context) : BaseDAO(context) {

    val hotelsParent: Observable<HotelsListParentResponse>
        get() = retrofit.create(HotelsRetrofit::class.java).hotels
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    fun getHotelDetail(id: String): Observable<HotelDetailParentResponse> {
        return retrofit.create(HotelsRetrofit::class.java).getHotelDetail(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    internal interface HotelsRetrofit {
        @get:GET("hotels")
        val hotels: Observable<HotelsListParentResponse>

        @GET("hotels/{id}")
        fun getHotelDetail(@Path("id") id: String): Observable<HotelDetailParentResponse>
    }
}
