package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GeoLocation implements Parcelable{

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("longitude")
	private double longitude;

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"GeoLocation{" + 
			"latitude = '" + latitude + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeDouble(this.latitude);
		dest.writeDouble(this.longitude);
	}

	public GeoLocation() {
	}

	protected GeoLocation(Parcel in) {
		this.latitude = in.readDouble();
		this.longitude = in.readDouble();
	}

	public static final Creator<GeoLocation> CREATOR = new Creator<GeoLocation>() {
		@Override
		public GeoLocation createFromParcel(Parcel source) {
			return new GeoLocation(source);
		}

		@Override
		public GeoLocation[] newArray(int size) {
			return new GeoLocation[size];
		}
	};
}