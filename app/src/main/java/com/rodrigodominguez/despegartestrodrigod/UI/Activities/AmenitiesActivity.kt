package com.rodrigodominguez.despegartestrodrigod.UI.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import com.rodrigodominguez.despegartestrodrigod.Constants
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.AmenitiesItem
import com.rodrigodominguez.despegartestrodrigod.R
import com.rodrigodominguez.despegartestrodrigod.UI.Adapters.AmenitiesAdapter
import kotlinx.android.synthetic.main.activity_amenities.*
import java.util.ArrayList

class AmenitiesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_amenities)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val reviewsItems = intent.extras!!.get(Constants.AMENITIES_EXTRA_KEY) as ArrayList<AmenitiesItem>

        val layoutManageramenities = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val adapteramenities = AmenitiesAdapter(reviewsItems)
        adapteramenities.setTypeRecycler(Constants.TYPE_ACTIVITY_AMENITIES)
        recycler_amenities!!.layoutManager = layoutManageramenities
        recycler_amenities!!.adapter = adapteramenities
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}