package com.rodrigodominguez.despegartestrodrigod.MVP.Contract


import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.HotelsListParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.ItemsItem

import io.reactivex.Observable
import io.reactivex.Observer

interface HotelsContract {
    interface presenter {
        fun onDestroy()
        fun getListHotels()
    }

    interface MainView<T> {
        fun showProgress()
        fun hideProgress()
        fun finishService()
        fun onSuccesResponse(t: T)
        fun onFailureResponse(error: String)
    }

    interface GetHotelsInteractor {
        val listHotelsParent: Observable<HotelsListParentResponse>

        fun getObserverListHotels(mainView: MainView<List<ItemsItem>>): Observer<HotelsListParentResponse>
    }

}
