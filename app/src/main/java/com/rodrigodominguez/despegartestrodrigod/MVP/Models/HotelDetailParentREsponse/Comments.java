package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Comments implements Parcelable{

	@SerializedName("bad")
	private String bad;

	@SerializedName("good")
	private String good;

	@SerializedName("type")
	private String type;

	protected Comments(Parcel in) {
		bad = in.readString();
		type = in.readString();
		good = in.readString();
	}

	public static final Creator<Comments> CREATOR = new Creator<Comments>() {
		@Override
		public Comments createFromParcel(Parcel in) {
			return new Comments(in);
		}

		@Override
		public Comments[] newArray(int size) {
			return new Comments[size];
		}
	};

	public void setBad(String bad){
		this.bad = bad;
	}

	public String getBad(){
		return bad;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
	public String toString() {
		return "Comments{" +
				"bad='" + bad + '\'' +
				", good='" + good + '\'' +
				", type='" + type + '\'' +
				'}';
	}

	public String getGood() {
		return good;
	}

	public void setGood(String good) {
		this.good = good;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(bad);
		parcel.writeString(type);
		parcel.writeString(good);
	}
}