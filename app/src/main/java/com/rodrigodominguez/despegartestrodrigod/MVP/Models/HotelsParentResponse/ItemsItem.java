package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemsItem{

	@SerializedName("amenities")
	private List<AmenitiesItem> amenities;

	@SerializedName("address")
	private String address;

	@SerializedName("price")
	private Price price;

	@SerializedName("name")
	private String name;

	@SerializedName("rating")
	private double rating;

	@SerializedName("main_picture")
	private String mainPicture;

	@SerializedName("id")
	private String id;

	@SerializedName("stars")
	private int stars;

	public void setAmenities(List<AmenitiesItem> amenities){
		this.amenities = amenities;
	}

	public List<AmenitiesItem> getAmenities(){
		return amenities;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setPrice(Price price){
		this.price = price;
	}

	public Price getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRating(double rating){
		this.rating = rating;
	}

	public double getRating(){
		return rating;
	}

	public void setMainPicture(String mainPicture){
		this.mainPicture = mainPicture;
	}

	public String getMainPicture(){
		return mainPicture;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStars(int stars){
		this.stars = stars;
	}

	public int getStars(){
		return stars;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"amenities = '" + amenities + '\'' + 
			",address = '" + address + '\'' + 
			",price = '" + price + '\'' + 
			",name = '" + name + '\'' + 
			",rating = '" + rating + '\'' + 
			",main_picture = '" + mainPicture + '\'' + 
			",id = '" + id + '\'' + 
			",stars = '" + stars + '\'' + 
			"}";
		}
}