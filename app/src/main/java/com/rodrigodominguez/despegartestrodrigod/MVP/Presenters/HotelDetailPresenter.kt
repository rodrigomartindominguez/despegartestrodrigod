package com.rodrigodominguez.despegartestrodrigod.MVP.Presenters

import android.content.Context

import com.rodrigodominguez.despegartestrodrigod.MVP.Contract.HotelDetailContract
import com.rodrigodominguez.despegartestrodrigod.MVP.Contract.HotelsContract
import com.rodrigodominguez.despegartestrodrigod.MVP.DAOs.HotelsDAO
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.HotelDetailParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.HotelsListParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.ItemsItem

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.observers.DisposableObserver


class HotelDetailPresenter(private val mContext: Context, private var mainView: HotelDetailContract.MainView<HotelDetailParentResponse>?) : HotelDetailContract.presenter, HotelDetailContract.GetHotelDetailInteractor {

    override fun onDestroy() {
        mainView = null
    }

    override fun getDetailHotel(id: String) {
        if (mainView != null)
            mainView!!.showProgress()
        this.getHotelDetailParent(id).subscribeWith(getObserverHotelDetail(mainView!!))
    }

    override fun getHotelDetailParent(id: String): Observable<HotelDetailParentResponse> {
        val hotelsDAO = HotelsDAO(mContext)

        return hotelsDAO.getHotelDetail(id)
    }

    override fun getObserverHotelDetail(mainView: HotelDetailContract.MainView<HotelDetailParentResponse>): Observer<HotelDetailParentResponse> {
        return object : DisposableObserver<HotelDetailParentResponse>() {
            override fun onNext(hotelDetailParentResponse: HotelDetailParentResponse) {
                if (mainView != null) {
                    mainView.hideProgress()

                    mainView.onSuccesResponse(hotelDetailParentResponse)
                }
            }

            override fun onError(e: Throwable) {
                if (mainView != null) {
                    mainView.hideProgress()
                    mainView.onFailureResponse(e.message!!)
                }
            }

            override fun onComplete() {
                mainView?.finishService()
            }
        }
    }
}
