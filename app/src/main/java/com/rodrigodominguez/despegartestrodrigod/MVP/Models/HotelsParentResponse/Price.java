package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse;

import com.google.gson.annotations.SerializedName;

public class Price{

	@SerializedName("final_price")
	private boolean finalPrice;

	@SerializedName("currency")
	private Currency currency;

	@SerializedName("best")
	private int best;

	@SerializedName("base")
	private int base;

	public void setFinalPrice(boolean finalPrice){
		this.finalPrice = finalPrice;
	}

	public boolean isFinalPrice(){
		return finalPrice;
	}

	public void setCurrency(Currency currency){
		this.currency = currency;
	}

	public Currency getCurrency(){
		return currency;
	}

	public void setBest(int best){
		this.best = best;
	}

	public int getBest(){
		return best;
	}

	public void setBase(int base){
		this.base = base;
	}

	public int getBase(){
		return base;
	}

	@Override
 	public String toString(){
		return 
			"Price{" + 
			"final_price = '" + finalPrice + '\'' + 
			",currency = '" + currency + '\'' + 
			",best = '" + best + '\'' + 
			",base = '" + base + '\'' + 
			"}";
		}
}