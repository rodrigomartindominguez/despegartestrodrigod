package com.rodrigodominguez.despegartestrodrigod

object Constants {

    val SERVER_URL = "https://private-a2ba2-jovenesdealtovuelo.apiary-mock.com/"
    val BUNDLE_ID_HOTEL = "BUNDLE_ID_HOTEL"
    val TYPE_LIST_AMENITIES = 4
    val TYPE_ACTIVITY_DETAIL_AMENITIES = 8
    val TYPE_ACTIVITY_AMENITIES = 16
    val IMAGE_COMMENTS_EXTRA_KEY = "IMAGE_COMMENTS_EXTRA_KEY"
    val AMENITIES_EXTRA_KEY = "AMENITIES_EXTRA_KEY"
}
