package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse;

import com.google.gson.annotations.SerializedName;

public class HotelDetailParentResponse{

	@SerializedName("price")
	private Price price;

	@SerializedName("hotel")
	private Hotel hotel;

	@SerializedName("id")
	private String id;

	public void setPrice(Price price){
		this.price = price;
	}

	public Price getPrice(){
		return price;
	}

	public void setHotel(Hotel hotel){
		this.hotel = hotel;
	}

	public Hotel getHotel(){
		return hotel;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"HotelDetailParentResponse{" + 
			"price = '" + price + '\'' + 
			",hotel = '" + hotel + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}