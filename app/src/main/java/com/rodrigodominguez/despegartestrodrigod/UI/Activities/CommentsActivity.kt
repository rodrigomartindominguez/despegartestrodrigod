package com.rodrigodominguez.despegartestrodrigod.UI.Activities

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import com.rodrigodominguez.despegartestrodrigod.Constants
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.ReviewsItem
import com.rodrigodominguez.despegartestrodrigod.R
import com.rodrigodominguez.despegartestrodrigod.UI.Adapters.CommentsAdapter
import com.rodrigodominguez.despegartestrodrigod.UI.Views.SeparatorDecoration
import kotlinx.android.synthetic.main.activity_comments.*
import java.util.ArrayList


class CommentsActivity : AppCompatActivity() {

    private var reviewsItems: ArrayList<ReviewsItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)


        if (intent.extras.get(Constants.IMAGE_COMMENTS_EXTRA_KEY) != null) {
            reviewsItems = intent.extras.get(Constants.IMAGE_COMMENTS_EXTRA_KEY) as ArrayList<ReviewsItem>
        }

        if (reviewsItems != null) {
            val adapter = CommentsAdapter(reviewsItems!!)

            recycler_comments!!.addItemDecoration(SeparatorDecoration(this, Color.LTGRAY, 1f, 12))

            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

            recycler_comments!!.layoutManager = layoutManager

            recycler_comments!!.adapter = adapter
        } else {
            showMessage()
        }
    }

    private fun showMessage() {
        sin_comentraios.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
