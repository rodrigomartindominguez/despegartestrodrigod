package com.rodrigodominguez.despegartestrodrigod.UI.Adapters

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.ItemsItem
import com.rodrigodominguez.despegartestrodrigod.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.rodrigodominguez.despegartestrodrigod.Constants.TYPE_LIST_AMENITIES

class HotelListAdapter(private val mContext: Context, private val itemsItems: List<ItemsItem>) : RecyclerView.Adapter<HotelListAdapter.ViewHolder>() {
    private var onClickHotelListener: OnClickHotelListener? = null

    fun setOnClickHotelListener(onClickHotelListener: OnClickHotelListener) {
        this.onClickHotelListener = onClickHotelListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotelListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_hotel_card, null)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: HotelListAdapter.ViewHolder, position: Int) {
        holder.bind(itemsItems[position])
    }

    override fun getItemCount(): Int {
        return itemsItems.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name_hotel: TextView
        var addres_hotel: TextView
        var rating: TextView
        var price_number: TextView
        var image_header: ImageView
        var recycler_starts: RecyclerView
        var recycler_amenities: RecyclerView
        private var adapterStart: RatingStartAdapter? = null
        private var adapteramenities: AmenitiesAdapter? = null

        init {
            name_hotel = itemView.findViewById(R.id.name_hotel)
            addres_hotel = itemView.findViewById(R.id.addres_hotel)
            rating = itemView.findViewById(R.id.rating)
            price_number = itemView.findViewById(R.id.price_number)
            image_header = itemView.findViewById(R.id.image_header)
            recycler_amenities = itemView.findViewById(R.id.recycler_amenities)
            recycler_starts = itemView.findViewById(R.id.recycler_starts)
        }

        fun bind(hotel: ItemsItem) {
            itemView.setOnClickListener { onClickHotelListener!!.onClickHotel(hotel) }

            name_hotel.text = hotel.name
            addres_hotel.text = hotel.address
            rating.text = hotel.rating.toString()
            val priceText = hotel.price.currency.mask + " " + hotel.price.base.toString()
            price_number.text = priceText

            val options = RequestOptions()
                    .centerCrop()
                    .priority(Priority.IMMEDIATE)

            Glide.with(mContext)
                    .load(hotel.mainPicture)
                    .apply(options)
                    .transition(withCrossFade())
                    .into(image_header)

            adapterStart = RatingStartAdapter(hotel.stars)
            val layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
            recycler_starts.layoutManager = layoutManager
            recycler_starts.adapter = adapterStart

            val layoutManageramenities = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)

            adapteramenities = AmenitiesAdapter(hotel.amenities)
            adapteramenities!!.setTypeRecycler(TYPE_LIST_AMENITIES)

            recycler_amenities.layoutManager = layoutManageramenities
            recycler_amenities.adapter = adapteramenities
        }
    }

    interface OnClickHotelListener {
        fun onClickHotel(itemsItem: ItemsItem)
    }
}