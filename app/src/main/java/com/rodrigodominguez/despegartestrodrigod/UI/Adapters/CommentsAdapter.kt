package com.rodrigodominguez.despegartestrodrigod.UI.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.ReviewsItem
import com.rodrigodominguez.despegartestrodrigod.R

class CommentsAdapter(private val itemsItems: List<ReviewsItem>) : RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_comment_card, null)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: CommentsAdapter.ViewHolder, position: Int) {
        holder.bind(itemsItems[position])
    }

    override fun getItemCount(): Int {
        return itemsItems.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name_user: TextView
        var comment_description: TextView
        var comment_bad_description: TextView

        init {
            name_user = itemView.findViewById(R.id.name_user)
            comment_description = itemView.findViewById(R.id.comment_description)
            comment_bad_description = itemView.findViewById(R.id.comment_bad_description)

        }

        fun bind(reviewItem: ReviewsItem) {
            name_user.text = StringBuilder().append(reviewItem.user.firstName).append(" ").append(reviewItem.user.lastName).toString()
            comment_description.text = reviewItem.comments.good
            comment_bad_description.text = reviewItem.comments.bad
        }
    }
}