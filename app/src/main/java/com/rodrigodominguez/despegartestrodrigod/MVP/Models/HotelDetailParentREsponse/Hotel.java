package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.AmenitiesItem;

import java.util.ArrayList;
import java.util.List;

public class Hotel implements Parcelable{

	@SerializedName("amenities")
	private ArrayList<AmenitiesItem> amenities;

	@SerializedName("address")
	private String address;

	@SerializedName("geo_location")
	private GeoLocation geoLocation;

	@SerializedName("reviews")
	private ArrayList<ReviewsItem> reviews;

	@SerializedName("city")
	private City city;

	@SerializedName("name")
	private String name;

	@SerializedName("rating")
	private double rating;

	@SerializedName("description")
	private String description;

	@SerializedName("main_picture")
	private String mainPicture;

	@SerializedName("id")
	private String id;

	@SerializedName("stars")
	private int stars;

	public void setAmenities(ArrayList<AmenitiesItem> amenities){
		this.amenities = amenities;
	}

	public ArrayList<AmenitiesItem> getAmenities(){
		return amenities;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setGeoLocation(GeoLocation geoLocation){
		this.geoLocation = geoLocation;
	}

	public GeoLocation getGeoLocation(){
		return geoLocation;
	}

	public void setReviews(ArrayList<ReviewsItem> reviews){
		this.reviews = reviews;
	}

	public ArrayList<ReviewsItem> getReviews(){
		return reviews;
	}

	public void setCity(City city){
		this.city = city;
	}

	public City getCity(){
		return city;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRating(double rating){
		this.rating = rating;
	}

	public double getRating(){
		return rating;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setMainPicture(String mainPicture){
		this.mainPicture = mainPicture;
	}

	public String getMainPicture(){
		return mainPicture;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStars(int stars){
		this.stars = stars;
	}

	public int getStars(){
		return stars;
	}

	@Override
 	public String toString(){
		return 
			"Hotel{" + 
			"amenities = '" + amenities + '\'' + 
			",address = '" + address + '\'' + 
			",geo_location = '" + geoLocation + '\'' + 
			",reviews = '" + reviews + '\'' + 
			",city = '" + city + '\'' + 
			",name = '" + name + '\'' + 
			",rating = '" + rating + '\'' + 
			",description = '" + description + '\'' + 
			",main_picture = '" + mainPicture + '\'' + 
			",id = '" + id + '\'' + 
			",stars = '" + stars + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(this.amenities);
		dest.writeString(this.address);
		dest.writeParcelable(this.geoLocation, flags);
		dest.writeTypedList(this.reviews);
		dest.writeParcelable(this.city, flags);
		dest.writeString(this.name);
		dest.writeDouble(this.rating);
		dest.writeString(this.description);
		dest.writeString(this.mainPicture);
		dest.writeString(this.id);
		dest.writeInt(this.stars);
	}

	public Hotel() {
	}

	protected Hotel(Parcel in) {
		this.amenities = new ArrayList<AmenitiesItem>();
		in.readList(this.amenities, AmenitiesItem.class.getClassLoader());
		this.address = in.readString();
		this.geoLocation = in.readParcelable(GeoLocation.class.getClassLoader());
		this.reviews = in.createTypedArrayList(ReviewsItem.CREATOR);
		this.city = in.readParcelable(City.class.getClassLoader());
		this.name = in.readString();
		this.rating = in.readDouble();
		this.description = in.readString();
		this.mainPicture = in.readString();
		this.id = in.readString();
		this.stars = in.readInt();
	}

	public static final Creator<Hotel> CREATOR = new Creator<Hotel>() {
		@Override
		public Hotel createFromParcel(Parcel source) {
			return new Hotel(source);
		}

		@Override
		public Hotel[] newArray(int size) {
			return new Hotel[size];
		}
	};
}