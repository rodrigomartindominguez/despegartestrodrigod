package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AmenitiesItem implements Parcelable{

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private String id;

	protected AmenitiesItem(Parcel in) {
		description = in.readString();
		id = in.readString();
	}

	public static final Creator<AmenitiesItem> CREATOR = new Creator<AmenitiesItem>() {
		@Override
		public AmenitiesItem createFromParcel(Parcel in) {
			return new AmenitiesItem(in);
		}

		@Override
		public AmenitiesItem[] newArray(int size) {
			return new AmenitiesItem[size];
		}
	};

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"AmenitiesItem{" + 
			"description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(description);
		dest.writeString(id);
	}
}