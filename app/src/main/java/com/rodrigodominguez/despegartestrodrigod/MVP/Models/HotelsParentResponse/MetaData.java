package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse;

import com.google.gson.annotations.SerializedName;

public class MetaData{

	@SerializedName("code")
	private String code;

	@SerializedName("message")
	private String message;

	@SerializedName("uow")
	private String uow;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setUow(String uow){
		this.uow = uow;
	}

	public String getUow(){
		return uow;
	}

	@Override
 	public String toString(){
		return 
			"MetaData{" + 
			"code = '" + code + '\'' + 
			",message = '" + message + '\'' + 
			",uow = '" + uow + '\'' + 
			"}";
		}
}