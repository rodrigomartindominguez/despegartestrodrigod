package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ReviewsItem implements Parcelable{

	@SerializedName("comments")
	private Comments comments;

	@SerializedName("user")
	private User user;

	public void setComments(Comments comments){
		this.comments = comments;
	}

	public Comments getComments(){
		return comments;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"ReviewsItem{" + 
			"comments = '" + comments + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.comments, flags);
		dest.writeParcelable(this.user, flags);
	}

	public ReviewsItem() {
	}

	protected ReviewsItem(Parcel in) {
		this.comments = in.readParcelable(Comments.class.getClassLoader());
		this.user = in.readParcelable(User.class.getClassLoader());
	}

	public static final Creator<ReviewsItem> CREATOR = new Creator<ReviewsItem>() {
		@Override
		public ReviewsItem createFromParcel(Parcel source) {
			return new ReviewsItem(source);
		}

		@Override
		public ReviewsItem[] newArray(int size) {
			return new ReviewsItem[size];
		}
	};
}