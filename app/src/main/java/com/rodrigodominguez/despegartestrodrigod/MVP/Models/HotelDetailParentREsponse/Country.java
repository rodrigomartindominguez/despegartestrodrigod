package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Country implements Parcelable{

	@SerializedName("code")
	private String code;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"Country{" + 
			"code = '" + code + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.code);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public Country() {
	}

	protected Country(Parcel in) {
		this.code = in.readString();
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Creator<Country> CREATOR = new Creator<Country>() {
		@Override
		public Country createFromParcel(Parcel source) {
			return new Country(source);
		}

		@Override
		public Country[] newArray(int size) {
			return new Country[size];
		}
	};
}