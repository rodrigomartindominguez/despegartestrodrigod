package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class City implements Parcelable{

	@SerializedName("country")
	private Country country;

	@SerializedName("code")
	private String code;

	@SerializedName("administrative_division")
	private AdministrativeDivision administrativeDivision;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setCountry(Country country){
		this.country = country;
	}

	public Country getCountry(){
		return country;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setAdministrativeDivision(AdministrativeDivision administrativeDivision){
		this.administrativeDivision = administrativeDivision;
	}

	public AdministrativeDivision getAdministrativeDivision(){
		return administrativeDivision;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"City{" + 
			"country = '" + country + '\'' + 
			",code = '" + code + '\'' + 
			",administrative_division = '" + administrativeDivision + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.country, flags);
		dest.writeString(this.code);
		dest.writeParcelable(this.administrativeDivision, flags);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public City() {
	}

	protected City(Parcel in) {
		this.country = in.readParcelable(Country.class.getClassLoader());
		this.code = in.readString();
		this.administrativeDivision = in.readParcelable(AdministrativeDivision.class.getClassLoader());
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Creator<City> CREATOR = new Creator<City>() {
		@Override
		public City createFromParcel(Parcel source) {
			return new City(source);
		}

		@Override
		public City[] newArray(int size) {
			return new City[size];
		}
	};
}