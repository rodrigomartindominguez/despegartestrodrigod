package com.rodrigodominguez.despegartestrodrigod.MVP.Presenters

import android.content.Context

import com.rodrigodominguez.despegartestrodrigod.MVP.Contract.HotelsContract
import com.rodrigodominguez.despegartestrodrigod.MVP.DAOs.HotelsDAO
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.HotelsListParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.ItemsItem

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.observers.DisposableObserver


class HotelsPresenter(private val mContext: Context, private var mainView: HotelsContract.MainView<List<ItemsItem>>?) : HotelsContract.presenter, HotelsContract.GetHotelsInteractor {


    override val listHotelsParent: Observable<HotelsListParentResponse>
        get() {
            val hotelsDAO = HotelsDAO(mContext)

            return hotelsDAO.hotelsParent
        }

    override fun onDestroy() {
        mainView = null
    }

    override fun getListHotels() {
        if (mainView != null)
            mainView!!.showProgress()
        this.listHotelsParent.subscribeWith(getObserverListHotels(mainView!!))
    }

    override fun getObserverListHotels(mainView: HotelsContract.MainView<List<ItemsItem>>): Observer<HotelsListParentResponse> {
        return object : DisposableObserver<HotelsListParentResponse>() {
            override fun onNext(hotelsListParentResponse: HotelsListParentResponse) {
                if (mainView != null) {
                    mainView.hideProgress()

                    mainView.onSuccesResponse(hotelsListParentResponse.items)
                }
            }

            override fun onError(e: Throwable) {
                if (mainView != null) {
                    mainView.hideProgress()
                    mainView.onFailureResponse(e.message!!)
                }

            }

            override fun onComplete() {
                mainView.finishService()
            }
        }
    }
}
