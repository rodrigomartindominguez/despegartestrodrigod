package com.rodrigodominguez.despegartestrodrigod.UI.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.rodrigodominguez.despegartestrodrigod.Constants
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.AmenitiesItem
import com.rodrigodominguez.despegartestrodrigod.R
import java.util.HashMap

class AmenitiesAdapter(private val amenities: List<AmenitiesItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var type = 4

    fun setTypeRecycler(type: Int) {
        this.type = type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (type == Constants.TYPE_LIST_AMENITIES) {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_amenities, null)
            return ViewHolder(v)
        } else if (type == Constants.TYPE_ACTIVITY_DETAIL_AMENITIES) {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_amenities_detail_activity, null)
            return ViewHolder(v)
        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_amenities_activity, null)
            return ViewHolderList(v)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (type == Constants.TYPE_LIST_AMENITIES || type == Constants.TYPE_ACTIVITY_DETAIL_AMENITIES)
            (holder as ViewHolder).bind(amenities[position])
        else
            (holder as ViewHolderList).bind(amenities[position])

    }

    override fun getItemCount(): Int {
        return amenities.size
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView

        init {
            imageView = itemView.findViewById(R.id.icon_amenities)
        }

        fun bind(amenitiesItem: AmenitiesItem) {
            mapIconDrawableInfo!![amenitiesItem.id]?.let { imageView.setImageResource(it) }
        }
    }

    internal inner class ViewHolderList(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView
        var title_amenitie: TextView

        init {
            imageView = itemView.findViewById(R.id.icon_amenities)
            title_amenitie = itemView.findViewById(R.id.title_amenitie)
        }

        fun bind(amenitiesItem: AmenitiesItem) {
            mapIconDrawableInfo[amenitiesItem.id]?.let { imageView.setImageResource(it) }
            title_amenitie.text = amenitiesItem.description
        }
    }

    companion object {
        private val mapIconDrawableInfo = createMap()

        private fun createMap(): Map<String, Int> {
            val myMap = HashMap<String, Int>()
            myMap.put("WIFI", R.drawable.baseline_wifi_24px)
            myMap.put("PISCN", R.drawable.baseline_pool_24px)
            myMap.put("BREAKFST", R.drawable.baseline_local_cafe_24px)
            myMap.put("PARKING", R.drawable.baseline_directions_car_24px)
            return myMap
        }
    }
}