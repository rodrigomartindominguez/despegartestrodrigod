package com.rodrigodominguez.despegarcomtest.MVP.DAOs

import android.content.Context
import com.rodrigodominguez.despegartestrodrigod.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

abstract class BaseDAO  {

    val context : Context
    val retrofit : Retrofit
    val canReturn : Boolean
    val url : String

    constructor(context: Context) {
        this.context = context
        this.canReturn = true
        this.url = Constants.SERVER_URL
        this.retrofit = buildRetrofit().build()
    }

    fun buildRetrofit() : Retrofit.Builder{
       var builder : OkHttpClient.Builder = OkHttpClient.Builder()

        return Retrofit.Builder().baseUrl(this.url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build())
    }





}