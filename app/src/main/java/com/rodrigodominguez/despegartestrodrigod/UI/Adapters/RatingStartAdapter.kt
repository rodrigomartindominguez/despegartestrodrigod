package com.rodrigodominguez.despegartestrodrigod.UI.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rodrigodominguez.despegartestrodrigod.R

class RatingStartAdapter(private val size: Int) : RecyclerView.Adapter<RatingStartAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingStartAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_start, null)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RatingStartAdapter.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {
        return size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}