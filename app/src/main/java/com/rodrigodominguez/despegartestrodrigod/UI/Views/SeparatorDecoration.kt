package com.rodrigodominguez.despegartestrodrigod.UI.Views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.View

/**
 * Created by it_rdominguez on 6/9/2018.
 */

class SeparatorDecoration(context: Context, color: Int, heightDp: Float, paddingDp: Int) : RecyclerView.ItemDecoration() {

    private val mPaint: Paint
    private val padding: Int

    init {
        mPaint = Paint()
        mPaint.color = color
        val thickness = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                heightDp, context.resources.displayMetrics)

        padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                paddingDp.toFloat(), context.resources.displayMetrics).toInt()
        mPaint.strokeWidth = thickness
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        val params = view.layoutParams as RecyclerView.LayoutParams

        // we want to retrieve the position in the list
        val position = params.viewAdapterPosition

        // and add a separator to any view but the last one
        if (position < state!!.itemCount) {
            outRect.set(0, 0, 0, mPaint.strokeWidth.toInt()) // left, top, right, bottom
        } else {
            outRect.setEmpty() // 0, 0, 0, 0
        }
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
        // we set the stroke width before, so as to correctly draw the line we have to offset by width / 2
        val offset = (mPaint.strokeWidth / 2).toInt()

        // this will iterate over every visible view
        for (i in 0 until parent.childCount) {
            // get the view
            val view = parent.getChildAt(i)
            val params = view.layoutParams as RecyclerView.LayoutParams

            // get the position
            val position = params.viewAdapterPosition

            // and finally draw the separator
            if (position < state!!.itemCount) {
                c.drawLine((view.left + padding).toFloat(), (view.bottom + offset).toFloat(), (view.right - padding).toFloat(), (view.bottom + offset).toFloat(), mPaint)
            }
        }
    }

}
