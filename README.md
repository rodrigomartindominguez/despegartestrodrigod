# Test Despegar - Rodrigo Dominguez

Proyecto desarrollado por Rodrigo Dominguez mail: rodrigomartind@gmail.com

## Descripción

El proyecto cuenta con cinco pantallas principales:

    MainActivity ->         Pantalla con el listado de hoteles
    DetailHotelActivity ->  Pantalla con el detalle del hotel seleccionado en la MainActivity.
    ImageDetailActivity ->  Pantalla con la imagen main del detalle del hotel, se accede mediante el click en la misma en la pantalla de Detalle.    
    CommentsActivity ->     Pantalla con el listado de comentarios de los usuarios.
    AmenitiesActivity ->    Pantalla con el listado de amenities del hotel seleccionado.
    
## Arquitectura

La arquitectura utilizada en el proyecto es la de MVP.

*Pendiente implementar Dagger


## Lenguaje

La mayoria de las clases del proyecto se encuentran en el lenguaje Kotlin.


## Librerias utilizadas
* **Librerias de soporte**

    implementation 'com.android.support:appcompat-v7:27.1.1'

    implementation 'com.android.support:support-compat:27.1.1'

    implementation 'com.android.support:cardview-v7:27.1.1'

    implementation 'com.android.support:recyclerview-v7:27.1.1'

* **ConstraintLayout**

    implementation 'com.android.support.constraint:constraint-layout:1.1.3'


* **RxJava**

    implementation 'io.reactivex.rxjava2:rxjava:2.2.0'

    implementation 'io.reactivex.rxjava2:rxandroid:2.1.0'
    
* **Retrofit**

    implementation 'com.squareup.retrofit2:retrofit:2.4.0'

    implementation 'com.squareup.retrofit2:converter-gson:2.3.0'

    implementation 'com.squareup.retrofit2:adapter-rxjava2:2.4.0'

* **Glide**

    implementation 'com.github.bumptech.glide:glide:4.8.0'
