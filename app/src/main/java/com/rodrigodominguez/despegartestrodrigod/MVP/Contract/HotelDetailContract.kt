package com.rodrigodominguez.despegartestrodrigod.MVP.Contract


import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelDetailParentREsponse.HotelDetailParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.HotelsListParentResponse
import com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse.ItemsItem

import io.reactivex.Observable
import io.reactivex.Observer

interface HotelDetailContract {
    interface presenter {
        fun onDestroy()
        fun getDetailHotel(id: String)
    }

    interface MainView<T> {
        fun showProgress()
        fun hideProgress()
        fun finishService()
        fun onSuccesResponse(t: T)
        fun onFailureResponse(error: String)
    }

    interface GetHotelDetailInteractor {
        fun getHotelDetailParent(id: String): Observable<HotelDetailParentResponse>

        fun getObserverHotelDetail(mainView: MainView<HotelDetailParentResponse>): Observer<HotelDetailParentResponse>
    }

}
