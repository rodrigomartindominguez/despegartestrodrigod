package com.rodrigodominguez.despegartestrodrigod.MVP.Models.HotelsParentResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HotelsListParentResponse{

	@SerializedName("meta_data")
	private MetaData metaData;

	@SerializedName("items")
	private List<ItemsItem> items;

	public void setMetaData(MetaData metaData){
		this.metaData = metaData;
	}

	public MetaData getMetaData(){
		return metaData;
	}

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	@Override
 	public String toString(){
		return 
			"HotelsListParentResponse{" + 
			"meta_data = '" + metaData + '\'' + 
			",items = '" + items + '\'' + 
			"}";
		}
}